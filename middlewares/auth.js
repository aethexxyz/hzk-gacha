var Auth = {
  is_login: (req,res,next)=>{
    if(!req.session.logged_in){
      return res.redirect('/login');
    }
    next();
  },
  is_admin: (req,res,next)=>{
    if(!req.session.is_admin){
      return res.redirect('/admin');
    }
    next();
  }
}

module.exports = Auth;
