var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var transactionSchema = new Schema({
  productName: {type: String, required: true},
  productImage: {type: String},
  rarity: {type: String},
  basePrice: {type: String}, 
  currentRating: {type: String},
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},  
  drawPrice: {type: Number},
  isTen: {type: Boolean}
},
{
  timestamps: true
});

var Transaction = mongoose.model('Transaction', transactionSchema, 'g_transaction');

module.exports = Transaction;
