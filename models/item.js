var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemSchema = new Schema({
  productName: {type: String, required: true},
  productImage: {type: String},
  productImage_publicId: {type: String},
  productImage_format: {type: String},
  rarity: {type: String, required: true},
  basePrice: {type: Number},
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  stock: {type: Number, required: true}
});

var Item = mongoose.model('Item', ItemSchema, 'g_item');

module.exports = Item;
