var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  circleName: {type: String, required: true},
  level: {type: Number, required: true}
  // level
  // 1 = admin
  // 2 = seller
},
{
  timestamps: true
});
var User = mongoose.model('User', userSchema, 'g_user');

module.exports = User;
