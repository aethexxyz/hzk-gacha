const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConfigSchema = new Schema({
  drawPrice: { type: Number, required: true},
  RRarity: { type: Number, required: true},
  SRRarity: { type: Number, required: true},
  SSRRarity: { type: Number, required: true},
  URRarity: { type: Number, required: true},
  URLimiter: { type: Number, required: true}
})

const Config = mongoose.model('Config', ConfigSchema, 'g_config');

module.exports = Config;