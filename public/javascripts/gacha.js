localStorage.ssrValue = 0;
var target = document.querySelector('h1');
var rateTarget = document.getElementById('rateTarget');
var addValue = document.getElementById('addValue');
var targetPrice = document.getElementById('targetPrice');
const voucherCheckbox = document.querySelector('.inputBox.checkBox input#useVoucher');
const freeCheckbox = document.querySelector('.inputBox.checkBox input#useFree');
var ssrValue = localStorage.ssrValue;
var realTarget = document.querySelector('#gachaResult ul#gachaTarget');
var liHTML = function(rarity, name, image, type, zonk){
  if(type === 'mono'){
    var view = '';
    var cl = ''
  } else {
    var view = 'opacity: 0';
    var cl = 'step-1';
  }

  if(zonk === 1){
    var rarityHTML = "";
    var zonkClass = "zonk";
  } else {
    var rarityHTML = "<span>"+rarity+"</span>";
    var zonkClass = "";

  }
  var content = "<li class='resultGacha "+cl+" "+zonkClass+"' style='"+view+"' type='"+type+"' rarity='"+rarity+"'> \
  "+image+" \
  <div class='text'> \
  <h4>"+name+"</h4> \
  "+rarityHTML+" \
  </div> \
  </li>";
  return content;
}

var gacha = {
  arRand: function(max) {
    var a = Math.round(Math.random() * max);
    return a;
  },
  random: function(){
    var rand = Math.random() * 100;
    return rand;
  },
  main: function(isTen){
    var rarityStatus = document.body.classList.value;
    if(rarityStatus === "rarityChanged"){
      var addPrice = document.getElementById('targetPrice').innerText;
      addPrice = addPrice.replace(/[a-z A-Z .]+/g, "") * 1;
    } else {
      addPrice = 0;
    }
    var random = this.random();
    function getValue(){
      var value= $.ajax({
        url: window.location.origin + '/api',
        async: false
      }).responseText;
      return value;
    };
    function getConfig(){
      let data = $.ajax({
        url: window.location.origin + '/api/config',
        async: false
      }).responseText;
      return data;
    }

    var arr = JSON.parse(getValue());
    const gachaConfig = JSON.parse(getConfig());
    const range = {
      ur: gachaConfig.URRarity,
      ssr: gachaConfig.URRarity + gachaConfig.SSRRarity,
      sr: gachaConfig.URRarity + gachaConfig.SSRRarity + gachaConfig.SRRarity
    }

    if (random <= range.ur && gachaConfig.URLimiter == false) {
      var x = this.arRand(arr.ur.length - 1);
      var result = arr.ur[x];
      result.isTen = isTen; 
      result.addPrice = addPrice;
      result.voucher = voucherCheckbox.checked || freeCheckbox.checked ? true : false;
      this.post(arr.ur[x]);
      return result;
    } else if (random > range.ur && random <= range.ssr + (localStorage.ssrValue*1)) {
      var x = this.arRand(arr.ssr.length - 1);
      var result = arr.ssr[x];
      result.isTen = isTen; 
      result.addPrice = addPrice;
      result.voucher = voucherCheckbox.checked || freeCheckbox.checked ? true : false;

      this.post(arr.ssr[x]);
      return result;
    } else if (random > range.ssr + (localStorage.ssrValue*1) && random <= range.sr + (localStorage.ssrValue*1)) {
      if(arr.sr.length !== 0){
        var x = this.arRand(arr.sr.length - 1);
        var result = arr.sr[x];
        result.isTen = isTen;
        result.addPrice = addPrice;
        result.voucher = voucherCheckbox.checked || freeCheckbox.checked ? true : false;

        this.post(arr.sr[x]);
        return result;
      } else {
        let result = {
          addPrice,
          isTen,
          voucher: voucherCheckbox.checked || freeCheckbox.checked,
          productName: 'ZONK',
          rarity: 'R',
          productImage: ''
        }
        this.post(result)
        return result;
      }
    } else if (random > range.sr + (localStorage.ssrValue * 1) && random <= 100) {
      if(arr.r.length !== 0){
        var x = this.arRand(arr.r.length - 1);
        var result = arr.r[x];
        result.isTen = isTen;
        result.addPrice = addPrice;
        // result.voucher = voucherCheckbox.checked
        result.voucher = voucherCheckbox.checked || freeCheckbox.checked ? true : false;

        this.post(arr.r[x]);
      } else {
        let result = {
          addPrice,
          isTen,
          voucher: voucherCheckbox.checked || freeCheckbox.checked,
          productName: 'ZONK',
          rarity: 'R',
          productImage: ''
        }
        this.post(result)

        return result;
      }
      return result;
    } else {
      let result = {
        addPrice,
        isTen,
        voucher: voucherCheckbox.checked || freeCheckbox.checked,
        productName: 'ZONK',
        rarity: 'R',
        productImage: ''
      }
      this.post(result)

      return result;

    }
  },
  post: function(item){
    $.ajax({

      type: "POST",
      url: window.location.origin + '/gachaDraw',
      contentType: "application/json",
      dataType: 'json',
      data: JSON.stringify(item)
    })
  },
  drawOne: function(){
    var result = this.main(false);
    // console.log(result)
    if(result.productImage){
      var imageHTML = "<img src='"+result.productImage+"'/>";
    } else {
      var imageHTML = "";
    }
    if(result.productName === "ZONK"){
      var isZonk = 1
    } else {
      var isZonk = 0
    }
    var html = liHTML(result.rarity, result.productName, imageHTML, 'mono', isZonk);

    loadingScreen.style.display = 'flex';
    loadingScreen.classList.remove('hide');
    loading.playLoading();
    setTimeout(function(){
      target.innerHTML = result;
      realTarget.insertAdjacentHTML('beforeend', html);
      $('.fullscreen#gachaResult').addClass('show');
      document.getElementById('gacha-1').addEventListener('ended', function(){
        $('video.gachaVideo#gacha-1').removeClass('show');
        $('video.gachaVideo#gacha-2').addClass('show');
        setTimeout(function(){
          $('video.gachaVideo#gacha-2').get(0).play();
        },1000)
        setTimeout(function(){
          $('#gachaResult .result').addClass('show');
        }, 2200);
      });
    }, 0);
  },
  drawTen: function(){
    var resultArray = [];
    var counter = counter || 0;
    for (var i = 0; i < 5; i++) {
      resultArray.push(this.main(true));
    }
    loading.playLoading();
    loadingScreen.style.display = 'flex';
    loadingScreen.classList.remove('hide');
    $('.fullscreen#gachaResult').addClass('show decaDraw');
    $('.fullscreen#gachaResult').attr('counter', 0);

    setTimeout(function(){
      for (var i = 0; i < resultArray.length; i++) {
        if(resultArray[i].productImage){
          var imageHTML = "<img src='"+resultArray[i].productImage+"'/>";
        } else {
          var imageHTML = "";
        }
        if(resultArray[i].productName === "ZONK"){
          var isZonk = 1
        } else {
          var isZonk = 0
        }
        var html = liHTML(resultArray[i].rarity, resultArray[i].productName, imageHTML, 'deca', isZonk);
        realTarget.insertAdjacentHTML('beforeend', html);

        document.getElementById('gacha-1').addEventListener('ended', function(){
          $('video.gachaVideo#gacha-1').removeClass('show');
          $('video.gachaVideo#gacha-2').addClass('show');
          setTimeout(function(){
            $('video.gachaVideo#gacha-2').get(0).play();
          },1000)
          setTimeout(function(){
            $('#gachaResult .result').addClass('show');
            $('#gachaResult .result button.backHome').css('display', 'none');
            $('#gachaResult .result ul li')[0].removeAttribute('style');
          }, 2200);
        });
      }
    }, 1000);
  }
}
voucherCheckbox.addEventListener('change', () => {
  if (voucherCheckbox.checked){
    document.querySelector('button#drawTen').setAttribute('disabled', 'disabled');
    document.querySelector('button.powerUp').setAttribute('disabled', 'disabled');
  } else {
    document.querySelector('button#drawTen').removeAttribute('disabled');
    document.querySelector('button.powerUp').removeAttribute('disabled');
  }
})
freeCheckbox.addEventListener('change', () => {
  if (freeCheckbox.checked){
    document.querySelector('button#drawTen').setAttribute('disabled', 'disabled');
    document.querySelector('button.powerUp').setAttribute('disabled', 'disabled');
  } else {
    document.querySelector('button#drawTen').removeAttribute('disabled');
    document.querySelector('button.powerUp').removeAttribute('disabled');
  }
})
var powerup = {
  Show: function(){
    var popup = document.querySelector('.popup#powerUp');
    if(popup){
      popup.style.display = 'flex';
    }
  },
  Close: function(){
    var popup = document.querySelector('.popup#powerUp');
    if(popup){
      popup.style.display = 'none';
    }
  },
  Up: function(){
    var currentVal = rateTarget.getAttribute('rate') * 1;
    if(currentVal < 100){
      currentVal += .5;
      rateTarget.setAttribute('rate', currentVal);
      rateTarget.innerHTML = currentVal + "%";
      addValue.innerHTML = "(+"+(currentVal - 1)+")";
      var price = (currentVal-1)*(3000*2);
      targetPrice.innerHTML = "Rp. "+price;
    }
  },
  Down: function(){
    var currentVal = rateTarget.getAttribute('rate') * 1;
    if(currentVal >= 1.5){
    currentVal -= .5;
    rateTarget.setAttribute('rate', currentVal);
    rateTarget.innerHTML = currentVal + "%";
    addValue.innerHTML = "(+"+(currentVal - 1)+")";
    var price = (currentVal-1)*(3000*2);
    targetPrice.innerHTML = "Rp. "+price;
    }
  },
  Change: function(){
    var currentVal = (rateTarget.getAttribute('rate') * 1) - 1;
    document.querySelector('button#drawTen').setAttribute('disabled', 'disabled');
    localStorage.ssrValue = currentVal;
    var currentSSR = document.getElementById('currentSSR'),
        currentSR = document.getElementById('currentSR'),
        currentR = document.getElementById('currentR');
    localStorage.ssrOutput = currentVal + 1;
    currentR.innerHTML = (90-localStorage.ssrOutput) + "%";
    currentSSR.innerHTML = localStorage.ssrOutput+"%";
    document.body.classList.add('rarityChanged');
    this.Close();
  }
}
