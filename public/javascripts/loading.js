var barParent = document.getElementById('loadingBarParent');
var barMove = document.getElementById('loadingBarMove');

var loadingScreen = document.getElementById('loadingScreen');
var loading = {
  playLoading: function(){
    $('video#bg').get(0).pause();
    var barWidth = barParent.offsetWidth;
    var plusValue = randomize(5, 20);
    var counter = 0;
    var pluser = function(){
      if(counter < 80){
        counter += plusValue;
        var calcOne = counter/100 * barWidth;
        barMove.style.width = calcOne+"px";
        barMove.setAttribute('count', counter+"%");
      } else {
        counter = 100;
        barMove.style.width = 100+"%";
        barMove.setAttribute('count', counter+"%");
        clearInterval(loopingInterval);
        setTimeout(function(){
          loadingScreen.classList.add('hide');
        }, 2000)
        setTimeout(function(){
          loadingScreen.style.display = 'none';
          loading.resetLoading();
        },2500)
        setTimeout(function(){
          $('#gachaResult .fade').addClass('white');
          $('video.gachaVideo#gacha-1').addClass('show');
        }, 3000)
        setTimeout(function(){
          $('video.gachaVideo#gacha-1').get(0).play();
          $('#gachaResult .fade').removeClass('white');
        },4000)
      }
    }
    var loopingInterval = setInterval(pluser, 250);
  },
  resetLoading: function(){
    barMove.removeAttribute('style');
    barMove.setAttribute('count', 0);
  }
}

var nextOnDeca = function(){
  var counter = $('.fullscreen#gachaResult').attr('counter') * 1;
  var liLength = $('.fullscreen#gachaResult ul li').length - 1;
  if(counter < liLength){
    counter++;
    $('.fullscreen#gachaResult').attr('counter', counter);
    var prev = counter-1;
    $('#gachaResult .result ul li').eq(prev).css('display', 'none');
    $('video.gachaVideo#gacha-2').get(0).load();
    setTimeout(function(){
      $('video.gachaVideo#gacha-2').get(0).play();
    },500);
    setTimeout(function(){
      $('#gachaResult .result ul li')[counter].removeAttribute('style');
    },1700)
  } else {
    $('video.gachaVideo#gacha-2').get(0).load();
    $('#gachaResult .result ul li').removeAttr('style');
    $('#gachaResult .result ul li .text').css('display', 'none');
    $('#gachaResult .result ul li').removeClass('step-1');
    $('#gachaResult .result ul li').addClass('step-2');
    $('.fullscreen.decaDraw#gachaResult ul#gachaTarget').addClass('full');
    $('#gachaResult .result button.backHome').removeAttr('style');
    $('#gachaResult .result button.next').css('display', 'none');
    $('.fullscreen.decaDraw#gachaResult .result h1').removeAttr('style');
  }

};

var randomize = function(min, max){
  var a = Math.round(Math.random() * (max-min) + min);
  if(a == 0){
    a += Math.round(Math.random() * (max-min) + min);
  }
  return a;
}
