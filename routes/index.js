var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Item = require('../models/item');
const ConfigGacha = require('../models/config');
var Transaction = require('../models/transaction');
var Auth_mdw = require('../middlewares/auth');
var crypto = require('crypto');
var cloudinary = require('cloudinary');
var secret = 'hzk';
var session_store;
/* GET home page. */

router.get('/', function (req, res, next) {
  var ssr = [0],
    sr = [0],
    r = [0];
  Item.find({}, (err, item) => {
    for (var i = 0; i < item.length; i++) {
      if (item[i].rarity === 'R') {
        r.push(item[i].stock);
      } else if (item[i].rarity === 'SR') {
        sr.push(item[i].stock);
      } else if (item[i].rarity === 'SSR') {
        ssr.push(item[i].stock);
      }
    }
    if (ssr.length < 1) {
      ssr.push(0);
    } else if (sr.length < 1) {
      sr.push(0);
    } else {
      r.push(0);
    }
    var stockCounter = {
      ssr: ssr.reduce((a, b) => a + b) || 0,
      sr: sr.reduce((a, b) => a + b) || 0,
      r: r.reduce((a, b) => a + b) || 0,
    }
    ConfigGacha.find({}).then(data => {
      const R = 100 - (data[0].URRarity + data[0].SSRRarity + data[0].SRRarity);
      const config = {
        URRarity: data[0].URRarity,
        SSRRarity: data[0].SSRRarity,
        SRRarity: data[0].SRRarity,
        RRarity: R
      }
      res.render('gacha', { stock: stockCounter, rarity: config });
    })
  })
});

router.get('/catalog', (req, res) => {
  Item.find({ productName: { $ne: "ZONK" } }, 'productName productImage_format productImage_publicId productImage rarity', (err, item) => {
    let imageArray = [];
    for (var i = 0; i < item.length; i++) {
      imageArray.push(cloudinary.url(item[i].productImage_publicId, {
        secure: true,
        height: 350,
        quality: 100,
        format: 'jpg'
      })
      );
    }

    res.render('catalog', { items: item, images: imageArray });
  }).sort({ rarity: -1 });
});
router.get('/api/catalog', (req, res) => {
  Item.find({
    productName: { $ne: "ZONK" },
    basePrice: { $gt: 0 },
    stock: { $gt: 0 }
  }, 'productName productImage_publicId productImage rarity basePrice user')
    .populate({ path: 'user', select: 'circleName' })
    .sort({ rarity: -1 })
    .then(item => {
      for (var i = 0; i < item.length; i++) {
        item[i].productImage = cloudinary.url(item[i].productImage_publicId, {
          secure: true,
          height: 350,
          quality: 100,
          format: 'jpg'
        })
      }
      res.json(item)
    })
})
router.post('/gachaDraw', (req, res, next) => {
  if (req.body._id) {
    Item.findOne({ _id: req.body._id }, (err, item) => {
      var currentQty = item.stock;
      item.stock = (currentQty * 1) - 1;
      item.save((err) => {
        ConfigGacha.find({}, (err, data) => {
          Transaction.find({}, (err, row) => {
            var transaction = new Transaction({
              productName: item.productName,
              productImage: item.productImage,
              rarity: item.rarity,
              user: item.user,
              basePrice: item.basePrice,
              drawPrice: req.body.voucher ? 0 : (req.body.addPrice + data[0].drawPrice),
              isTen: req.body.isTen
            });
            transaction.save((err) => {
              if (err) throw err;
              res.status(201).end('{"success": "Updated successfully!"}');
            })
          })
        })
      })
    });
  } else {
    ConfigGacha.find({}, (err, data) => {
      Transaction.find({}, (err, row) => {
        let transaction = new Transaction({
          productName: 'ZONK',
          rarity: 'R',
          basePrice: 0,
          drawPrice: req.body.voucher ? 0 : (req.body.addPrice + data[0].drawPrice),
          isTen: req.body.isTen
        })

        transaction.save((err) => {
          if (err) throw err;
          res.status(201).end('{"success": "Updated successfully!"}');
        })
      })
    })
  }
})

router.get('/transaction', Auth_mdw.is_login, (req, res, next) => {
  session_store = req.session;
  // var cost = [];
  var income = [];
  Transaction
    .find({})
    .populate('user')
    .then(row => {
      for (var i = 0; i < row.length; i++) {
        income.push(row[i].drawPrice * 1);
        // cost.push(row[i].basePrice * 1);
      }
      if (income.length > 0) {
        // var f_cost = cost.reduce((a, b) => a + b);
        var f_income = income.reduce((a, b) => a + b);
      } else {
        // var f_cost = 0;
        var f_income = 0;
      }
      // var bersih = f_income - f_cost;

      const allDrawNoZonk = row.filter(i => {
        if (i.user) {
          return i
        }
      }).length
      const userDraw = row.filter(i => {
        if (i.user && i.productName !== 'ZONK') {
          return i.user._id == session_store.userId
        }
      })
      let costArray = []
      userDraw.map(i => {
        costArray.push(i.basePrice * 1)
      });
      // const userCost = costArray.reduce((a, b) => a + b);
      const currentPercent = userDraw.length / allDrawNoZonk * 100;
      const myProfit = f_income * currentPercent / 100;

      const profit = {
        myDraw: userDraw.length,
        // userCost,
        currentPercent: parseFloat(currentPercent).toFixed(2),
        myProfit: (parseFloat(myProfit).toFixed() * 1).toLocaleString()
      }
      res.render('transaction', {
        item: row,
        income: f_income,
        // cost: f_cost,
        // bersih: bersih,
        profit,
        title: "[hZk] Draw History"
      });
    })
    .catch(err => {
      console.log(err)
    })
})

router.get('/login', (req, res, next) => {
  res.render('login', { title: "[hZk] Login" });
})

router.get('/api', (req, res, next) => {
  var ur = [],
    ssr = [],
    sr = [],
    r = [];
  Item.find({ stock: { $gte: 1 } }, 'productName productImage user rarity stock', (err, row) => {
    for (var i = 0; i < row.length; i++) {
      var rare = row[i].rarity;
      if (rare == 'UR') {
        ur.push(row[i])
      }
      else if (rare === 'SSR') {
        ssr.push(row[i]);
      } else if (rare === 'SR') {
        sr.push(row[i]);
      } else {
        r.push(row[i]);
      }
    }
    var api = { ur, ssr, sr, r }
    // console.log(api);
    res.json(api);
  })
})

router.get('/api/config', (req, res) => {
  ConfigGacha.find({}, 'RRarity SRRarity SSRRarity URRarity URLimiter', (err, data) => {
    Transaction.find({}, (err, t) => {
      const config = {
        RRarity: data[0].RRarity,
        SRRarity: data[0].SRRarity,
        SSRRarity: data[0].SSRRarity,
        URRarity: data[0].URRarity,
        URLimiter: t.length >= data[0].URLimiter ? false : true
      }
      res.status(200).json(config);
    })
  })
})

router.post('/login', (req, res, next) => {
  session_store = req.session;
  var password = crypto.createHmac('sha256', secret).update(req.param('password')).digest('hex');
  if (req.body.username == "" || req.body.password == "") {
    req.flash('info', 'Username/Password empty!');
    res.redirect('/login');
  } else {
    User.find({
      username: req.body.username,
      password: password
    }, (err, user) => {
      if (err) throw err;
      if (user.length > 0) {
        session_store.username = req.body.username;
        session_store.userId = user[0]._id;
        session_store.logged_in = true;
        session_store.is_admin = user[0].level === 1 ? true : false;
        res.redirect('/admin');
      } else {
        req.flash('info', 'Username/Password incorrect!');
        res.redirect('/login');
      }
    });
  }
});

router.get('/logout', (req, res, next) => {
  req.session.destroy((err) => {
    if (err) {
      console.log("ERROR");
    } else {
      res.redirect('/login');
    }
  });
});

module.exports = router;
