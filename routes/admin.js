var express = require('express');
var router = express.Router();
var Auth_mdw = require('../middlewares/auth');
var Item = require('../models/item');
var User = require('../models/user');
var ConfigGacha = require('../models/config');
var fs = require('fs');
var crypto = require('crypto');

var cloudinary = require('cloudinary');
var session_store;

cloudinary.config({
  cloud_name: 'hzk-storage',
  api_key: '829858291927973',
  api_secret: 'Ro18kvlk2fvo6fRnNQG3GtFIWDc'
});


router.get('/', Auth_mdw.is_login, function(req, res, next) {
  session_store = req.session;
  // console.log(session_store)
  // Item.find({user: session_store.userId}, (err, item)=>{
  Item
  .find({}, 'productName productImage rarity basePrice stock user')
  .populate('user')
  .sort({user: -1})
  .then(item => {
    res.render('admin', {
      session_store: session_store, 
      items: item, 
      title: "[hZk] Stock"
    });
  })
  .catch(err => {
    console.log(err)
  }) 
  // Item.find({}, (err, item)=>{

  // }).select('productName productImage rarity basePrice stock user').sort({user: -1});

});

router.get('/configuration', Auth_mdw.is_login, Auth_mdw.is_admin, (req,res) => {
  session_store = req.session;
  ConfigGacha.find({}, (err, data) => {
    res.render('admin/configuration', {
      session_store,
      data: data[0],
      title: "[hZk] Configuration"
    })
  })
});

router.put('/configuration', Auth_mdw.is_login, (req,res) => {
  session_store = req.session;
  const { drawPrice, SRRarity, SSRRarity, URRarity, URLimiter } = req.body;
  ConfigGacha.find({}, (err, data) => {
    data[0].drawPrice = drawPrice;
    data[0].SRRarity = SRRarity;
    data[0].SSRRarity = SSRRarity;
    data[0].URRarity = URRarity;
    data[0].URLimiter = URLimiter;
    data[0].save(err => {
      if(err){
        req.flash('info', 'Failed to save')
      } else {
        req.flash('info', 'Configuration saved')
      }
      res.redirect('/admin/configuration');
    })
  })
})
router.get('/account', Auth_mdw.is_login, (req,res) => {
  session_store = req.session;
  User.find({_id: session_store.userId}, 'circleName').then(data => {
    res.render('admin/account', {
      session_store,
      data: data[0],
      title: "[hzk] Account Settings"
    });
  })
  .catch(err => {
    console.log(err)
  })
});
router.put('/account', Auth_mdw.is_login, (req,res) => {
  session_store = req.session;
  var secret = 'hzk';
  const { circleName, password } = req.body;
  var passOut = crypto.createHmac('sha256', secret).update(password).digest('hex');

  if(circleName !== '' && password !== ''){
    User.findById(session_store.userId).then(data => {
      data.circleName = circleName;
      data.password = passOut;
      data.save(err => {
        if(err){
          req.flash('info', 'Failed to save')
        } else {
          req.flash('info', 'Account updated')
        }
        res.redirect('/admin/account');
      })
    })
  } else {
    req.flash('info', 'Circle name or password must be filled')    
    res.redirect('/admin/account')
  }
})
router.get('/edit/(:id)', Auth_mdw.is_login, (req,res,next)=>{
  session_store = req.session;
  Item.findOne({_id: req.params.id}, (err,item)=>{
    // console.log(item.productName);
    res.render('admin/edit', {
      session_store: session_store,
      item: item,
      title: "[hZk] Edit "+item.productName,
    });
  }).select('productName productImage rarity basePrice stock');
})

router.put('/edit/(:id)', Auth_mdw.is_login, (req,res,next)=>{
  session_store = req.session;

  Item.findById(req.params.id, (err, item)=>{

    item.productName = req.body.productName
    item.rarity = req.body.rarity
    item.basePrice = req.body.basePrice
    item.stock = req.body.stock

    item.save((err)=>{
      if(err){
        req.flash('info', 'Gagal')

      } else {
        req.flash('info', 'Berhasil')
      }
      res.redirect('/admin/edit/'+req.params.id);

    })
  })
})

router.delete('/delete/(:id)', Auth_mdw.is_login, (req,res,next)=>{
  session_store = req.session;
  Item.findOne({_id: req.params.id},(err, row)=>{
    // console.log(row.productImage);
    if(row.productImage !== null){
      cloudinary.v2.uploader.destroy(row.productImage_publicId, function(error, result){
        console.log(result, error)
      });
    } else {
      res.redirect('/admin')
    }
  });
  Item.findOneAndDelete({_id: req.params.id}, (err)=>{
    if(err){
      req.flash('info','Gagal');
      res.redirect('/admin');

    } else {
      req.flash('info', 'Berhasil');
      res.redirect('/admin');

    }
  });

});

router.get('/add',Auth_mdw.is_login, (req,res,next)=>{
  session_store = req.session;
  res.render('admin/add', {
    session_store: session_store,
    action: 'add',
    title: "[hZk] Add Item"
  });
});

router.post('/add', Auth_mdw.is_login, (req,res,next)=>{
  session_store = req.session;
  var imageURL;

  if(req.body.productName == "" || req.body.rarity == "" || req.files.productImage == null){
    req.flash('info', "Some field are required");
    res.redirect('/admin/add');
  } else {
    let itemImage = req.files.productImage;
    var path = './public/uploads/';
    var itemImageName = Date.now() +"_"+itemImage.name;
    if(itemImageName !== ''){
      itemImage.mv(path + itemImageName, (err)=>{
        if(err){
          console.log("ERORR MESSAGE:"+err);
        } else {
          cloudinary.v2.uploader.upload(path + itemImageName,
          function(error, result){
            var itemContent = new Item({
              productName: req.body.productName,
              productImage: result.secure_url,
              productImage_publicId: result.public_id,
              productImage_format: result.format,
              rarity: req.body.rarity,
              user: session_store.userId,
              basePrice: req.body.basePrice,
              stock: req.body.stock
            });
            itemContent.save((err)=>{
              if(err) throw err;
              req.flash('info', "Input "+req.body.productName+" success!");
              res.redirect('/admin/add');
            })
            fs.unlinkSync(path + itemImageName);
          });
        }
      });
    }
  }
});


module.exports = router;
